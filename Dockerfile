# =====================================
# Base image for different google fonts
# -------------------------------------

FROM alpine:edge

# Install build dependencies
RUN apk add --no-cache --update --virtual .builddeps \
                                    curl \
                                    zip \
                                    fontconfig


WORKDIR /workspace

# Install all packaged fonts
ADD required_fonts.txt /workspace/required_fonts.txt

# Download and unpack
RUN cat /workspace/required_fonts.txt | xargs -I {} sh -c ' \
            export _ORIGINAL_FONT=$(echo "{}" | tr " " "+") && \
            export _LOWER_FONT=$(echo "{}" | tr "[:upper:]" "[:lower:]" | tr " " "_") && \
                curl https://fonts.google.com/download?family=$_ORIGINAL_FONT \
                    -o /tmp/$_LOWER_FONT.zip && \
                $(mkdir -p /usr/share/fonts/ttf-$_LOWER_FONT/ || true) && \
                unzip /tmp/$_LOWER_FONT.zip -d /usr/share/fonts/ttf-$_LOWER_FONT/ && \
                rm -rf /tmp/$_LOWER_FONT.zip'


# List and cleanup
RUN fc-list && rm -rf  /workspace/required_fonts.txt && apk del .builddeps
