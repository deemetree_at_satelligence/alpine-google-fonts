# :mountain_cableway: alpine-google-fonts

One Alpine docker as a source for popular google fonts.

## Font source
Downloading fonts from the official google font repository: https://fonts.google.com/?sort=popularity 

### Default included fonts

- [Roboto](https://fonts.google.com/specimen/Roboto)
- [Open Sans](https://fonts.google.com/specimen/Open+Sans)
- [Lato](https://fonts.google.com/specimen/Lato)
- [Montserrat](https://fonts.google.com/specimen/Montserrat)

## Usage

Can be used as a base image in multistage builds for an easy supply of fonts.  

```dockerfile
# <Dockerfile>

#### Font supply image
FROM deemetreeats11/alpine-google-fonts AS font_supply

RUN ls /usr/share/fonts/ && echo "done!"

### Runtime stage
FROM alpine:edge AS runtime

# Font copy
COPY --from=font_supply /usr/share/fonts/ /usr/share/fonts/

# Optional validation
RUN apk add fontconfig && fc-list && apk del fontconfig
# Output:
#   /usr/share/fonts/ttf-roboto/Roboto-Light.ttf: Roboto,Roboto Light:style=Light,Regular
#   /usr/share/fonts/ttf-montserrat/Montserrat-Italic.ttf: Montserrat:style=Italic
#   ...

RUN echo "done!"
```

## Building your own

Clone the repo, modify `required_fonts.txt` document with needed font families, run docker build.

```bash
$ clone git@gitlab.com:deemetree_at_satelligence/alpine-google-fonts.git

$ cd alpine-google-fonts

# Let's suppose we want to install only Oswald
# https://fonts.google.com/specimen/Oswald
$ echo Oswald > required_fonts.txt

$ docker build -t $USER/alpine-google-fonts .
```